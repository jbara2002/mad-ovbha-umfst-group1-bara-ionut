package com.geoni.projectlabfinal;

import android.location.Location;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class RequestAssistanceActivity extends AppCompatActivity {

    private LocationTracker locationTracker;
    private BackendService backendService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request_assistance);

        // Set up the Retrofit instance
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://your-backend-service-url.com/api/")
                .client(new OkHttpClient())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        backendService = retrofit.create(BackendService.class);

        // Set up the Request Assistance button
        Button requestAssistanceButton = findViewById(R.id.requestAssistanceButton);
        requestAssistanceButton.setOnClickListener(v -> {
            EditText messageEditText = findViewById(R.id.requestAssistanceMessage);
            String message = messageEditText.getText().toString();

            // Send an SOS signal with the user's location and message
            if (locationTracker == null) {
                Toast.makeText(RequestAssistanceActivity.this, "Unable to get location.", Toast.LENGTH_SHORT).show();
                return;
            }

            Location location = locationTracker.getLocation();
            if (location == null) {
                Toast.makeText(RequestAssistanceActivity.this, "Unable to get location.", Toast.LENGTH_SHORT).show();
                return;
            }

            double latitude = location.getLatitude();
            double longitude = location.getLongitude();

            SOSRequest request = new SOSRequest(message, latitude, longitude);
            Call<Void> call = backendService.sendSOS(request);
            call.enqueue(new Callback<Void>() {
                @Override
                public void onResponse(@NonNull Call<Void> call, @NonNull Response<Void> response) {
                    Toast.makeText(RequestAssistanceActivity.this, "SOS signal sent.", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onFailure(@NonNull Call<Void> call, @NonNull Throwable t) {
                    Toast.makeText(RequestAssistanceActivity.this, "Failed to send SOS signal.", Toast.LENGTH_SHORT).show();
                }
            });
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (locationTracker == null) {
            locationTracker = new LocationTracker(this);
            locationTracker.start();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (locationTracker != null) {
            locationTracker.stop();
        }
    }
}
