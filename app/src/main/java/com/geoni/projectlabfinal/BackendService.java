package com.geoni.projectlabfinal;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface BackendService {
    @POST("sos")
    Call<Void> sendSOS(@Body SOSRequest request);
}
