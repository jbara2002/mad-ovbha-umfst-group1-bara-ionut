package com.geoni.projectlabfinal;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Set up the Request Assistance button
        Button requestAssistanceButton = findViewById(R.id.requestAssistanceButton);
        requestAssistanceButton.setOnClickListener(v -> {
            Intent intent = new Intent(MainActivity.this, activity_request_assistance.class);
            startActivity(intent);
        });

        // Set up the Emergency Services button
        // Will call emergency Services such 112 or 911
        Button emergencyServicesButton = findViewById(R.id.emergencyServicesButton);
        emergencyServicesButton.setOnClickListener(v -> {
            Intent intent = new Intent(Intent.ACTION_DIAL);
            intent.setData(Uri.parse("tel:911"));
            startActivity(intent);
        });

        // Set up the Roadside Assistance button

        Button roadsideAssistanceButton = findViewById(R.id.roadsideAssistanceButton);
        roadsideAssistanceButton.setOnClickListener(v -> {
            Intent intent = new Intent(Intent.ACTION_DIAL);
            intent.setData(Uri.parse("tel:555-1234"));
            startActivity(intent);
        });

        // Set up the Nearby Services Button
        Button nearbyServicesButton = findViewById(R.id.nearbyServicesButton);
        nearbyServicesButton.setOnClickListener(v -> {
            // Launch the nearby services activity
            Intent intent = new Intent(this, nearbyServicesActivity.class);
            startActivity(intent);
        });

    }
}
