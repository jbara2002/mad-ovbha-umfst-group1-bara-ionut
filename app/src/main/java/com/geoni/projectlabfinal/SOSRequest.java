package com.geoni.projectlabfinal;


public class SOSRequest {
    private String message;
    private double latitude;
    private double longitude;

    public SOSRequest(String message, double latitude, double longitude) {
        this.message = message;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public String getMessage() {
        return message;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }
}


