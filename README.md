# MAD OVBHA UMFST Group1 Bara Ionut

The Ovbha app is an Android app designed to provide assistance to users in emergency situations. The app features several buttons that allow users to request different types of assistance, such as roadside assistance and emergency services. In addition, the app also includes a feature that allows users to send an SOS signal with their location and a message to a backend service.  
This project is made for the feedback between me and the laboratory teacher.

Main Activity
The app's main activity (MainActivity) features the following buttons:

Request Assistance Button: Allows users to request assistance from the app.  
Emergency Services Button: Allows users to call emergency services such as 911 or 112.  
Roadside Assistance Button: Allows users to call a roadside assistance service.  
Nearby Services Button: Launches the nearbyServicesActivity class that displays a list of nearby services.  
Request Assistance Activity
The app's activity_request_assistance class allows users to send an SOS signal with their location and a message to a backend service. This activity features the following components:  

Location Tracker: Tracks the user's location using the LocationTracker class.  
Retrofit Instance: Configures a Retrofit instance to communicate with the backend service.  
Location TextView: Displays the user's current location.  
Request Assistance Button: Sends an SOS signal with the user's location and message to the backend service.  
To use the Request Assistance feature, the user needs to grant the app permission to access their location. After the user clicks on the Request Assistance button, the app uses the LocationTracker class to get the user's current location. The app then sends an SOS signal with the user's location and message to the backend service using Retrofit. If the SOS signal is sent successfully, the app displays a success message. If the SOS signal fails to send, the app displays an error message.  

#Project Status 
Meets next requiments 
The app uses GPS to pinpoint your location and then sends out an SOS signal to a preselected list of contact. The contacts can then track your location. (Finalized)
whether that's coming to pick you up or calling a tow truck. (Finalized)

The app can also include a database of nearby service stations, hotels, and restaurants, so you can find help even if you're in an unfamiliar area. (not implemented now, maybe next week)